Assalamu 'alaykum warahmatullahi wabarakatuh


# Setup


1. Get AutoHotKey: https://www.autohotkey.com/
2. Get & open my script: https://gitlab.com/AbdurahmanTumgaev/censorstuff/-/raw/main/CensorStuff.ahk?inline=false
3. When pressing shortcut Ctrl-Space-c, then moving mouse, releasing shortcut, creates a black box where you moved.
4. Remove box by pressing and releasing Ctrl-Space-c without moving mouse.



If you need another shortcut, or something else, contact me
