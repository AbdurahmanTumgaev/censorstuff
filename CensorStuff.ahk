#SingleInstance Force
CoordMode, Mouse, Screen

global br := ""

#If GetKeyState("LCtrl")
space & c::
    MouseGetPos, xMouseStart, yMouseStart
    KeyWait, c
    MouseGetPos, xMouseEnd, yMouseEnd

    if(br != "")
        DestroyBR(br)

    xDiff := Abs(xMouseEnd - xMouseStart)
    yDiff := Abs(yMouseEnd - yMouseStart)

    if xDiff > 10 && yDiff > 10 ; If the beginning mouse pos was more upper left than the end
        br := BlackRect(Min(xMouseStart, xMouseEnd), Min(yMouseStart, yMouseEnd), xDiff, yDiff)
return
#if

BlackRect(x, y, w, h) {
	static n := 0
	n++
	Gui, %n%:-Caption +AlwaysOnTop +ToolWindow
	Gui, %n%:Color, Black
	Gui, %n%:Show, x%x% y%y% w%w% h%h%
	return n
}

DestroyBR(ByRef n) {
	Gui, %n%:Destroy
	n := ""
}
